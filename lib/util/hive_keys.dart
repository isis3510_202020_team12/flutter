class HiveKeys {
  static const String USER = "user";
  static const String APPOINTMENTS = "appointments";
  static const String DIAGNOSIS = "diagnosis";
  static const String TREATMENTS = "treatments";
}