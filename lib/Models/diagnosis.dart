import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:medrecord/Models/treatment.dart';

class Diagnosis{
  String _description;
  bool _isCurrent;
  String _name;
  Timestamp _registrationDate;
  List<Treatment> _treatments;

  Diagnosis({
    description,
    isCurrent,
    name,
    registrationDate,
    treatments
  }){
    this._description = description;
    this._isCurrent = isCurrent == null ? true : isCurrent;
    this._name = name;
    this._registrationDate = registrationDate;
    this._treatments = treatments;
  }

  factory Diagnosis.fromJson( Map<String,dynamic> json ) {
    return Diagnosis(
      description : json["description"],
      isCurrent : json["isCurrent"],
      name : json["name"],
      registrationDate : json["registrationDate"] is String ? Timestamp.fromDate( (DateTime.parse( json[ "registrationDate" ])) ) : json[ "registrationDate" ],
      treatments: json[ "treatments" ] != null ?  List<Treatment>.from( json[ "treatments" ].map( ( treatment ) => Treatment.fromJson( treatment )) ) : null
    );
  }

  Map<String, dynamic> toCollectionJson() => {
    "description" : this._description,
    "isCurrent" : this._isCurrent,
    "name" : this._name,
    "registrationDate" : this._registrationDate,
  };

  Map<String, dynamic> toPersistenceJson() => {
    "description" : this._description,
    "isCurrent" : this._isCurrent,
    "name" : this._name,
    "registrationDate" : this._registrationDate.toString(),
    "treatments" : [ for( var treatment in this._treatments ) treatment.toPersistenceJson() ]
  };


  List<Treatment> get treatments => this._treatments;
  Timestamp get registrationDate => this._registrationDate;
  String get name => this._name;
  bool get isCurrent => this._isCurrent;
  String get description =>this._description;

  set treatments(List<Treatment> value) {
    this._treatments = value;
  }

  set registrationDate(Timestamp value) {
    this._registrationDate = value;
  }

  set name(String value) {
    this._name = value;
  }

  set isCurrent(bool value) {
    this._isCurrent = value;
  }

  set description(String value) {
    this._description = value;
  }

  void addTreatment( Treatment treatment ){
    this._treatments == null ? this._treatments = [ treatment ] : this._treatments.add( treatment );
  }

}