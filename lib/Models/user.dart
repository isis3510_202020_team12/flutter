import 'package:medrecord/Models/appointment.dart';
import 'package:medrecord/Models/diagnosis.dart';

class User {
  String _uuid;
  String _fullname;
  int _age;
  int _height;
  int _weight;
  String _bloodType;
  String _photo;
  List<Appointment> _appointments;
  List<Diagnosis> _diagnosis;

  User({
    fullname,
    age,
    height,
    weight,
    bloodType,
    photo,
    appointments,
    diagnoses
  }){
    this._fullname = fullname;
    this._age = age;
    this._height = height;
    this._weight = weight;
    this._bloodType = bloodType;
    this._photo = photo;
    this._appointments = appointments;
    this._diagnosis = diagnoses;
  }

  factory User.fromJson( Map<String, dynamic> json ) {
    return User(
      fullname : "${json["name"]} ${json["lastName"]}",
      age : json["age"],
      height : json["height"] ,
      weight : json["weight"],
      bloodType : json["blood"],
      photo : json["image"],
      appointments : json["appointments"] != null ?  List<Appointment>.from( json["appointments"].map( ( appointment ) => Appointment.fromJson( appointment )) ) : null,
      diagnoses : json["diagnosis"] != null ? List<Diagnosis>.from( json["diagnosis"].map( ( diagnose ) => Diagnosis.fromJson( diagnose )) ) : null
    );
  }

  Map<String,dynamic> toCollectionJson() => {

    "fullname"  : this._fullname,
    "age"  : this._age,
    "height"  : this._height,
    "weight"  : this._weight,
    "bloodType"  : this._bloodType,
    "photo"  : this._photo,
  };
  
  Map<String,dynamic> toPersistenceJson() => {
    "fullname"  : this._fullname,
    "age"  : this._age,
    "height"  : this._height,
    "weight"  : this._weight,
    "bloodType"  : this._bloodType,
    "photo"  : this._photo,
    "appointments"  : [ for( var appointment in this._appointments ) appointment.toPersistenceJson() ],
    "diagnoses"  : [ for( var diagnosis in this._diagnosis ) diagnosis.toPersistenceJson() ],
  };

  String get uuid => this._uuid;
  String get fullname => this._fullname;
  int get age => this._age;
  int get height => this._height;
  int get weight => this._weight;
  String get bloodType => this._bloodType;
  String get photo => this._photo;
  List<Appointment> get appointments => this._appointments;
  List<Diagnosis> get diagnosis => this._diagnosis;

  set diagnosis(List<Diagnosis> value) {
    this._diagnosis = value;
  }

  set uuid (String value) {
    this._uuid = value;
  }

  set bloodType(String value) {
    this._bloodType = value;
  }

  set weight(int value) {
    this._weight = value;
  }

  set height(int value) {
    this._height = value;
  }

  set age(int value) {
    this._age = value;
  }

  set fullname(String value) {
    this._fullname = value;
  }

  set appointments( List<Appointment> value){
    this._appointments = value;
  }

  set photo (String value){
    this._photo = value;
  }
}
