import 'package:medrecord/Models/geometry.dart';

class Place {
  final String name;
  final double rating;
  final int userRatingCount;
  final String vicinity;
  final Geometry geometry;

  Place(
      {this.geometry,
      this.name,
      this.rating,
      this.userRatingCount,
      this.vicinity});

  Place.fromJson(Map<dynamic, dynamic> json)
      : name = json['name'],
        rating = (json['rating'] != null) ? json['rating'].toDouble() : null,
        userRatingCount = (json['user_ratings_total'] != null)
            ? json['user_ratings_total']
            : null,
        vicinity = json['vicinity'],
        geometry = Geometry.fromJson(json['geometry']);
}
