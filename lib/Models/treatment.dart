import 'package:cloud_firestore/cloud_firestore.dart';

class Treatment{
  
  String _comments;
  String _dosage;
  int _dosesTaken;
  int _totalDoses;
  String _duration;
  String _name;
  Timestamp _prescriptionDate;
  Timestamp _startDate;
  String _times;
  String _type;

  Treatment({
    comments,
    dosage,
    dosesTaken,
    totalDoses,
    duration,
    name,
    prescriptionDate,
    startDate,
    times,
    type
  }) {
    this._comments = comments;
    this._dosage = dosage;
    this._dosesTaken = dosesTaken;
    this._totalDoses = totalDoses;
    this._duration = duration;
    this._name = name;
    this._prescriptionDate = prescriptionDate;
    this._startDate = startDate;
    this._times = times;
    this._type = type;
  }
  factory Treatment.fromJson( Map<String,dynamic> json ) {
    return Treatment(
      comments : json['comments'],
      dosage : json['dosage'],
      dosesTaken : json['dosesTaken'],
      totalDoses : json['totalDoses'],
      duration : json['duration'],
      name : json['name'],
      prescriptionDate : json["registrationDate"] is String ? Timestamp.fromDate( (DateTime.parse( json[ "registrationDate" ])) ) : json[ "registrationDate" ],
      startDate : json["startDate"] is String ? Timestamp.fromDate( (DateTime.parse( json[ "startDate" ])) ) : json[ "startDate" ],
      times : json['times'],
      type : json['type']
    );
  }

  Map<String,dynamic> toCollectionJson() => {
    'comments' : this._comments,
    'dosage' : this._dosage,
    'dosesTaken' : this._dosesTaken,
    'totalDoses' : this._totalDoses,
    'duration' : this._duration,
    'name' : this._name,
    "prescriptionDate" : this._prescriptionDate,
    "startDate" : this._startDate,
    'times' : this._times,
    'type' : this._type
  };

  Map<String,dynamic> toPersistenceJson() => {
    'comments' : this._comments,
    'dosage' : this._dosage,
    'dosesTaken' : this._dosesTaken,
    'totalDoses' : this._totalDoses,
    'duration' : this._duration,
    'name' : this._name,
    "prescriptionDate" : this._prescriptionDate.toString(),
    "startDate" : this._startDate.toString(),
    'times' : this._times,
    'type' : this._type
  };



  String get type => this._type;
  String get times => this._times;
  Timestamp get startDate => this._startDate;
  Timestamp get prescriptionDate => this._prescriptionDate;
  String get name => this._name;
  String get duration => this._duration;
  int get totalDoses => this._totalDoses;
  int get dosesTaken => this._dosesTaken;
  String get dosage => this._dosage;
  String get comments => this._comments;

  set type(String value) {
    _type = value;
  }

  set times(String value) {
    _times = value;
  }

  set startDate(Timestamp value) {
    _startDate = value;
  }

  set prescriptionDate(Timestamp value) {
    _prescriptionDate = value;
  }

  set name(String value) {
    _name = value;
  }

  set duration(String value) {
    _duration = value;
  }

  set totalDoses(int value) {
    _totalDoses = value;
  }

  set dosesTaken(int value) {
    _dosesTaken = value;
  }

  set dosage(String value) {
    _dosage = value;
  }

  set comments(String value) {
    _comments = value;
  }
}