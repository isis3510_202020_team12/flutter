import 'package:cloud_firestore/cloud_firestore.dart';

class Appointment{
  Timestamp _date;
  String _hospital;
  String _type;


  Appointment(this._date, this._hospital, this._type );

  factory Appointment.fromJson( Map<String,dynamic> json ){
    return Appointment(
        json["date"] is String ? Timestamp.fromDate( (DateTime.parse( json[ "date" ])) ) : json[ "date" ],
        json["hospital"],
        json["type"],
     );
  }

  Map<String,dynamic> toPersistenceJson() => {
    "date" : this._date.toString(),
    "hospital" : this._hospital,
    "type" : this._type
  };

  Timestamp get date => this._date;
  String get hospital => this._hospital;
  String get type => this._type;


  set date(Timestamp value) {
    _date = value;
  }

  set hospital(String value) {
    _hospital = value;
  }

  set type(String value) {
    _type = value;
  }

}