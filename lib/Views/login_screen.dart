import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:medrecord/Managers/data_manager.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'medical_basic_info_screen_form.dart';
import 'package:provider/provider.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'home.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String _email = "";
  String _password = "";
  final scaffoldKey = GlobalKey<ScaffoldState>();
  FirebaseAuth auth = FirebaseAuth.instance;

  @override
  Widget build(BuildContext context) {
    auth = FirebaseAuth.instance;
    final user = Provider.of<UserViewModel>(context);
    auth.authStateChanges().listen((User pUser) {

    });

    return Scaffold(
        key: scaffoldKey,
        body: Container(
          padding: const EdgeInsets.only(top: 80, bottom: 30, right: 40, left: 40),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Center(
                  child: Image.asset(
                    'assets/logo.png',
                    width: 120,
                    height: 120,
                    fit: BoxFit.fitWidth,
                  ),
                ),
                Center(
                  child: Text(
                    "Welcome Back",
                    style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 38,
                        color: Theme.of(context).primaryColor),
                  ),
                ),
                Center(
                  child: Text(
                    "Please login to continue",
                    style: TextStyle(
                        fontWeight: FontWeight.w200,
                        fontSize: 15,
                        color: Theme.of(context).primaryColor),
                  ),
                ),
                SafeArea(child: Container(height: 150)),
                TextField(
                    keyboardType: TextInputType.emailAddress,
                    decoration: InputDecoration(
                        icon: Icon(LineAwesomeIcons.user),
                        hintText: "example@email.com",
                        labelText: "Email"),
                    onChanged: (pEmail) {
                      setState(() {
                        this._email = pEmail;
                      });
                    }),
                TextField(
                  obscureText: true,
                  decoration: InputDecoration(
                      icon: Icon(LineAwesomeIcons.lock),
                      hintText: "********",
                      labelText: "Password"),
                  onChanged: (pPassword) {
                    setState(() {
                      this._password = pPassword;
                    });
                  },
                ),
                Container(
                  height: 60,
                  width: 250,
                  margin: const EdgeInsets.only(top: 50),
                  child: RaisedButton(
                    child: Text(
                      "LOGIN",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 20),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: Theme.of(context).primaryColor,
                    onPressed: () async {
                      var connectivityResult = await ( Connectivity().checkConnectivity() );
                      if ( connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi ){
                        await _login(context, user)
                            ? Navigator.push(context,
                            MaterialPageRoute(builder: (context) => Home()))
                            : showDialog<void>(
                          context: context,
                          barrierDismissible: true,
                          builder: (BuildContext context) {
                            return _alertUser( context, "No matches found!", "Please enter a valid email and password" );
                          },
                        );
                      }
                      else{
                        final snackbar = SnackBar(
                          content: Text( "No conection, please try again later." ),
                          behavior: SnackBarBehavior.floating,
                        );
                        scaffoldKey.currentState.showSnackBar( snackbar );
                      }
                    },
                  ),
                ),
                Container(
                    padding: const EdgeInsets.all(20),
                    child: RichText(
                      text: TextSpan(
                        style: TextStyle( fontSize: 20, color: Colors.black12 ),
                        children: <TextSpan>[
                          TextSpan(text: "Don't have an account? " ),
                          TextSpan(
                              text: "Register",
                              style: TextStyle( fontSize: 20, color: Colors.black12, decoration: TextDecoration.underline ),
                              recognizer: TapGestureRecognizer()
                                ..onTap = () async {
                                  var connectivityResult = await ( Connectivity().checkConnectivity() );
                                  if ( connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi ) {
                                    Alert(
                                        context: context,
                                        title: "Register",
                                        content: Column(
                                          children: <Widget>[
                                            TextField(
                                              decoration: InputDecoration(
                                                icon: Icon(
                                                    Icons.account_circle),
                                                labelText: 'Email',
                                              ),
                                              onChanged: (value) {
                                                setState(() {
                                                  this._email = value;
                                                });
                                              },
                                            ),
                                            TextField(
                                              obscureText: true,
                                              decoration: InputDecoration(
                                                icon: Icon(Icons.lock),
                                                labelText: 'Password',
                                              ),
                                              onChanged: (value) {
                                                setState(() {
                                                  this._password = value;
                                                });
                                              },
                                            ),
                                          ],
                                        ),
                                        buttons: [
                                          DialogButton(
                                            onPressed: () async {
                                              Navigator.pop(context);
                                              await _register(context) ?
                                              showDialog<void>(
                                                context: context,
                                                barrierDismissible: true,
                                                builder: (
                                                    BuildContext context) {
                                                  return _alertUser(
                                                      context, "Success!",
                                                      "Your account has been created, please fill the following information to continue");
                                                },
                                              )
                                                  :
                                              showDialog<void>(
                                                context: context,
                                                barrierDismissible: true,
                                                builder: (
                                                    BuildContext context) {
                                                  return _alertUser(context,
                                                      "Could not create account",
                                                      "Please enter a valid email and password");
                                                },
                                              );
                                            },
                                            child: const Text(
                                              "REGISTER",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontSize: 20),
                                            ),
                                          )
                                        ]).show();
                                  }
                                  else{
                                    final snackbar = SnackBar(
                                      content: const Text( "No conection, please try again later." ),
                                      behavior: SnackBarBehavior.floating,
                                    );
                                    scaffoldKey.currentState.showSnackBar( snackbar );
                                  }
                                }),
                        ],
                      ),
                    )
                )
              ],
            ),
          ),
        )
    );

  }



  Widget _alertUser( BuildContext context, String title, String message ){
    return AlertDialog(
      title: Text( title ),
      content: Text( message ),
      //content: Icon(LineAwesomeIcons.exclamation_triangle, color: Colors.red, size: 60,),
      actions: <Widget>[
        TextButton(
          child: const Text(
            'OK',
            style: TextStyle(fontSize: 20),
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ],
    );
  }

  Future<bool> _register( BuildContext context ) async {
    try {
      await FirebaseAuth.instance.createUserWithEmailAndPassword(
        email: this._email,
        password: this._password,
      );
      Navigator.push(context, MaterialPageRoute(builder: (context) => MedicalBasicInfoForm()));
      return true;
    }
    on FirebaseAuthException catch (e, s) {
      if (e.code == 'user-not-found') {
        FirebaseCrashlytics.instance.recordError(e, s);
      } else if (e.code == 'wrong-password') {
        FirebaseCrashlytics.instance.recordError(e, s);
      }
      return false;
    }
  }


  Future<bool> _login(BuildContext context, UserViewModel user ) async {
    try {
      UserCredential userCredential = await FirebaseAuth.instance.signInWithEmailAndPassword(
              email: this._email, password: this._password
      );
      await DataManager.fetchAndSaveData( user, userCredential.user.uid.toString() );
      return true;

    } on FirebaseAuthException catch (e,s) {
        FirebaseCrashlytics.instance.recordError( e, s );
      return false;
    } on SocketException catch (e,s) {
      FirebaseCrashlytics.instance.recordError( e, s );
      return false;
    }
    catch( e,s ){
      FirebaseCrashlytics.instance.recordError( e, s );
      return false;
    }
  }
}

