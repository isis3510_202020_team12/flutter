import 'dart:io';

import 'package:flutter/material.dart';
import 'widgets/place_search_map.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';

class ServicesScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.only( top: 10, left: 20, right: 20 ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 10),
              child: Text(
                "Services",
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 35,
                    color: Theme.of(context).primaryColor),
              ),
            ),
            Container(
              child: Column(
                children: [
                  Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: ButtonTheme(
                        minWidth: double.infinity,
                        height: 70,
                        child: RaisedButton.icon(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          padding: const EdgeInsets.only(right: 80),
                          elevation: 6,
                          onPressed: () async {
                            await checkConnection()
                                ? Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MapSearch()))
                                : showDialog<void>(
                                    context: context,
                                    barrierDismissible: true,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: const Text("No Internet Connection!"),
                                        content: const Text(
                                            "To use this feature you should be connected to internet"),
                                        actions: <Widget>[
                                          TextButton(
                                            child: const Text(
                                              'OK',
                                              style: TextStyle(fontSize: 20),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                          },
                          label: const Text(
                            'Find nearby hospitals',
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 20),
                          ),
                          icon: Icon(
                            LineAwesomeIcons.hospital,
                            size: 35,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      )),
                  Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: ButtonTheme(
                        minWidth: double.infinity,
                        height: 70,
                        child: RaisedButton.icon(
                          color: Colors.white,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          padding: EdgeInsets.only(right: 135),
                          elevation: 6,
                          onPressed: () {},
                          label: const Text(
                            'Call ambulance',
                            style: TextStyle(
                                fontWeight: FontWeight.w600, fontSize: 20),
                          ),
                          icon: Icon(
                            LineAwesomeIcons.ambulance,
                            size: 35,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ))
                ],
              ),
            ),
            Container(
              margin: const EdgeInsets.only(bottom: 10, top: 15),
              child: Text(
                "Health",
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 35,
                    color: Theme.of(context).primaryColor),
              ),
            ),
            _health(context)
          ],
        ),
      ),
    ));
  }
}

Widget _health(BuildContext context) {
  return Container(
    child: Column(
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 15),
          child: Card(
            elevation: 8,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Container(
              padding: const EdgeInsets.all(15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Icon(
                          LineAwesomeIcons.heartbeat,
                          size: 30,
                        ),
                      ),
                      Text("Pulse",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 25,
                          ))
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 45),
                        child: Text("85",
                            style: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 30,
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: Text("bt/min",
                            style: const TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            )),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 15),
          child: Card(
            elevation: 8,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Container(
              padding: const EdgeInsets.all(15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Icon(
                          LineAwesomeIcons.stethoscope,
                          size: 30,
                        ),
                      ),
                      Text("Presure",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 25,
                          ))
                    ],
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 45),
                        child: const Text("170",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 30,
                            )),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: const Text("mm Hg",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            )),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 15),
          child: Card(
            elevation: 8,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(12),
            ),
            child: Container(
              padding: const EdgeInsets.all(15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: Icon(
                          LineAwesomeIcons.thermometer,
                          size: 30,
                        ),
                      ),
                      const Text("Body Temperature",
                          style: TextStyle(
                            fontWeight: FontWeight.w700,
                            fontSize: 25,
                          ))
                    ],
                  ),
                  Row(
                    children: [
                      const Padding(
                        padding: EdgeInsets.only(left: 45),
                        child: Text("36,8",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 30,
                            )),
                      ),
                      const Padding(
                        padding: EdgeInsets.only(left: 15),
                        child: Text("°C",
                            style: TextStyle(
                              fontWeight: FontWeight.w400,
                              fontSize: 20,
                            )),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ),
      ],
    ),
  );
}

Future<bool> checkConnection() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      return true;
    }
  } on SocketException catch (_) {
    return false;
  }
  return false;
}
