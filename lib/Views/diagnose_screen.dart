import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:medrecord/Models/treatment.dart';
import 'package:provider/provider.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:percent_indicator/percent_indicator.dart';
import '../Models/diagnosis.dart';

class DiagnoseScreen extends StatelessWidget{

  final int index;

  DiagnoseScreen( this.index );
  @override
  Widget build(BuildContext context) {
    UserViewModel user = Provider.of<UserViewModel>( context );
    Diagnosis diagnose = user.diagnosis[ index ];

    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 60, left: 20, right: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Diagnose",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 35,
                  color: Theme.of(context).primaryColor) ,
            ),
            Container(
              padding: const EdgeInsets.only( top: 10, bottom: 30 ),
              child: _diagnoseCard( context, diagnose )
            ),
            Text(
              "Treatments",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 30,
                  color: Theme.of(context).primaryColor) ,
            ),
            Expanded(
              child: ListView.builder(
                itemCount: diagnose.treatments.length,
                itemBuilder: ( BuildContext context, int treatmentIndex ){
                  return Container(
                      padding: EdgeInsets.only( top: 5, bottom: 30 ),
                      child: _treatmentCard( context, diagnose.treatments[ treatmentIndex ] )
                  );
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _diagnoseCard( BuildContext context, Diagnosis diagnose ){
    return Card(
        elevation: 8,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
        ),
        child: Container(
          padding: const EdgeInsets.only(left: 20,top: 5,right: 20,bottom: 10),
          child: Column(
            children: [
              Container(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: Text(
                        diagnose.name,
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 25,
                        ),
                      ),
                    ),
                    Flexible(
                      child: diagnose.isCurrent ?? true ?
                      const Text(
                        "current",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.red),
                      )
                          : const Text(
                        "closed",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.green
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Row(
                children: [
                  diagnose.registrationDate != null ?
                  Flexible(
                    child: Text(
                        "Registered ${DateFormat.yMMMd().format( DateTime.parse( diagnose.registrationDate.toDate().toString() ) )}",
                        style: TextStyle(fontWeight: FontWeight.w400, fontSize: 13, color: Colors.grey)
                    ),
                  )
                      :
                  Container()
                ],
              ),
              Container(
                padding: const EdgeInsets.only( top: 5, bottom: 5 ),
                child: const Divider(),
              ),
              Container(
                child: Text(
                  diagnose.description ?? ""
                ),
              )
            ],
          ),
        )
    );
  }

  Widget _treatmentCard( BuildContext context, Treatment treatment ){
    return Card(
        elevation: 8,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20)
        ),
        child: Container(
          padding: const EdgeInsets.only(left: 20,top: 25,right: 20,bottom: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                treatment.name ?? "",
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  fontSize: 25,
                ),
              ),
              Text(
                  treatment.type ?? "",
                  style: const TextStyle(fontWeight: FontWeight.w400, fontSize: 13, color: Colors.grey)
              ),
              Container(
                padding: const EdgeInsets.only( top: 5, bottom: 5 ),
                child: const Divider(),
              ),
              Container(
                    child: Row(
                      children: [
                        Expanded(
                          child: Column(
                            children: [
                              const Text(
                                "Dosage",
                                style: TextStyle (fontWeight: FontWeight.w400, fontSize: 15, color: Colors.grey )
                              ),
                              Text(
                                  treatment.dosage ?? 0,
                                  style: TextStyle( fontWeight: FontWeight.w400, fontSize: 15 )
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              const Text(
                                  "Duration",
                                  style: TextStyle (fontWeight: FontWeight.w400, fontSize: 15, color: Colors.grey )
                              ),
                              Text(
                                  treatment.duration ?? "",
                                  style: TextStyle( fontWeight: FontWeight.w400, fontSize: 15 )
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Column(
                            children: [
                              const Text(
                                  "Started",
                                  style: TextStyle (fontWeight: FontWeight.w400, fontSize: 15, color: Colors.grey )
                              ),
                              treatment.startDate != null ?
                              Text(
                                  DateFormat.yMMMd().format(DateTime.parse( treatment.startDate.toDate().toString())),
                                  style: TextStyle( fontWeight: FontWeight.w400, fontSize: 15 )
                              )
                              :
                              Container(),
                            ],
                          ),
                        )
                      ],
                    ),
              ),
              Container(
                margin: const EdgeInsets.only( top: 20 ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                        "Times",
                        style: TextStyle (fontWeight: FontWeight.w400, fontSize: 15, color: Colors.grey )
                    ),
                    Text(
                        treatment.times ?? "",
                        style: TextStyle( fontWeight: FontWeight.w400, fontSize: 15 )
                    ),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only( top: 20 ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const Text(
                        "Comments",
                        style: TextStyle (fontWeight: FontWeight.w400, fontSize: 15, color: Colors.grey )
                    ),
                    Text(
                        treatment.comments ?? "",
                        style: TextStyle( fontWeight: FontWeight.w400, fontSize: 15 )
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only( top: 20, bottom: 5 ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Flexible(
                      child: const Text(
                        "Progress",
                        style: TextStyle (fontWeight: FontWeight.w400, fontSize: 15, color: Colors.grey )
                      ),
                    ),
                    Flexible(
                      child: Text(
                        "${treatment.dosesTaken ?? 0}/${treatment.totalDoses ?? 0} doses",
                        style: TextStyle (fontWeight: FontWeight.w400, fontSize: 15, color: Colors.grey )
                      )
                    )
                  ],
                ),
              ),
              treatment.dosesTaken != null && treatment.totalDoses != null ?
              Row(
                children: [
                  Expanded(
                    child: LinearPercentIndicator(
                      lineHeight: 14.0,
                      percent: treatment.totalDoses != 0 ? treatment.dosesTaken/treatment.totalDoses : 0 ,
                      backgroundColor: Colors.black12,
                      progressColor: Colors.green,
                    ),
                  )
                ],
              )
                  :
              Container()
            ],
          )
        )
    );
  }
}