import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:medrecord/Views/medical_basic_info_screen_form.dart';
import 'package:provider/provider.dart';
import 'package:cached_network_image/cached_network_image.dart';


import 'diagnose_form_screen.dart';
import 'diagnose_screen.dart';

class RecordScreen extends StatelessWidget {
  Map<String,Color> weightStatusColoMap = {
    UserViewModel.OBESE : Colors.red,
    UserViewModel.OVERWEIGHT : Colors.orangeAccent,
    UserViewModel.NORMAL : Colors.green,
    UserViewModel.UNDERWEIGHT : Colors.orangeAccent
  };
  @override
  Widget build(BuildContext context) {
    UserViewModel user = Provider.of<UserViewModel>( context );
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(top: 10, left: 20, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: const EdgeInsets.only(bottom: 20),
              child: Text(
                "Medical Card",
                style: TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 40,
                    color: Theme.of(context).primaryColor),
              ),
            ),
            _medicalCard(context),
            Container(
              margin: const EdgeInsets.only(top: 20),
              child: Text(
                "Diagnosis",
                style: TextStyle(
                    fontWeight: FontWeight.w600,
                    fontSize: 30,
                    color: Theme.of(context).primaryColor),
              ),
            ),
            user.user != null ?
            Expanded(
              child: user.diagnosis != null ? _diagnosis( context ) : Container(),
            )
                :
            Container()
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(Icons.add),
        onPressed: () async {
          var connectivityResult = await ( Connectivity().checkConnectivity() );
          if ( connectivityResult != ConnectivityResult.mobile && connectivityResult != ConnectivityResult.wifi ){
            showDialog<void>(
                context: context,
                barrierDismissible: true,
                builder: (
                    BuildContext context) {
                  return AlertDialog(
                    title: const Text( "No connection" ),
                    content: const Text( "Please try again later" ),
                    //content: Icon(LineAwesomeIcons.exclamation_triangle, color: Colors.red, size: 60,),
                    actions: <Widget>[
                      TextButton(
                        child: const Text(
                          'OK',
                          style: TextStyle(fontSize: 20),
                        ),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                }
            );
          }
          else {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => DiagnoseFormScreen()));
          }
        },
      ),
    );
  }

  Widget _diagnosis(BuildContext context) {
    UserViewModel user = Provider.of<UserViewModel>( context );

    return ListView.builder(
      itemCount: user?.diagnosis?.length ?? 0,
      itemBuilder: (BuildContext context, int index) {
        return Container(
          padding: const EdgeInsets.all(5),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(20),
            ),
            elevation: 5,
            child: ListTile(
                onTap: () => {
                  Navigator.push( context, MaterialPageRoute(builder: ( context ) => DiagnoseScreen( index )))
                },
                title: Text(
                  user.diagnosis[index]?.name ?? "",
                  style: const TextStyle(
                    fontWeight: FontWeight.w700,
                    fontSize: 20,
                  ),
                ),
                subtitle: user.diagnosis[index]?.isCurrent ?? true
                    ? const Text(
                        "current",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.red),
                      )
                    : const Text(
                        "closed",
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            fontSize: 15,
                            color: Colors.green),
                      )
            ),
          ),
        );
      },
    );
  }

  Widget _medicalCard(BuildContext context) {
    final user = Provider.of<UserViewModel>(context);
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(20),
        ),
        elevation: 8,
        child: Container(
          padding: const EdgeInsets.all(20),
          child: Column(
            children: <Widget>[
              Container(
                padding: const EdgeInsets.only(bottom: 15),
                child: ListTile(
                  leading: user.user != null && user.photo != null ?
                  CircleAvatar(
                    backgroundColor: Theme.of(context).primaryColor,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                      child: CachedNetworkImage(
                        imageUrl: user.photo ,
                        placeholder: (context, url) => CircularProgressIndicator(),
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ),
                    )
                  )
                  :
                  const SizedBox( width: 20,),
                  title: Text(
                    user.user != null ? user.fullname ?? "" :"",
                    style: TextStyle(
                        fontWeight: FontWeight.w600,
                        color: Theme.of(context).primaryColor,
                        fontSize: 25),
                  ),
                  trailing: IconButton(
                    icon: Icon(
                      Icons.more_vert,
                      size: 35,
                      ),
                    onPressed: () => Navigator.push( context, MaterialPageRoute( builder: (context) => MedicalBasicInfoForm() ) ),
                  ),
                ),

              ),
              Container(
                margin: const EdgeInsets.only(bottom: 15.0, left: 16),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Row(
                      children: [
                        Icon(
                          LineAwesomeIcons.hourglass,
                          size: 35,
                        ),
                        Container(
                          margin: EdgeInsets.only(left: 10),
                          child: Column(
                            children: [
                              Text(
                                "${user.user != null ? user.age ?? "-" :"-" } years",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 15),
                              ),
                              const Text(
                                "Age",
                                style: TextStyle(
                                    fontWeight: FontWeight.w300, fontSize: 12),
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
                    Expanded(
                        child: Row(
                      children: [
                        Icon(
                          LineAwesomeIcons.ruler_vertical,
                          size: 30,
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            children: [
                              Text(
                                "${user.user != null ? user.height ?? "-" :"-"} m ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 15),
                              ),
                              const Text(
                                "Height",
                                style: TextStyle(
                                    fontWeight: FontWeight.w300, fontSize: 12),
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
                  ],
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 16),
                child: Row(
                  children: <Widget>[
                    Expanded(
                        child: Row(
                      children: [
                        Icon(
                          LineAwesomeIcons.weight,
                          size: 35,
                        ),
                        Container(
                          padding: const EdgeInsets.only(left: 10),
                          child: Column(
                            children: [
                              Text(
                                "${user.user != null ? user.weight ?? "-" :"-"} kg",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 15),
                              ),
                              const Text(
                                "Weight",
                                style: TextStyle(
                                    fontWeight: FontWeight.w300, fontSize: 12),
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
                    Expanded(
                        child: Row(
                      children: [
                        Icon(
                          LineAwesomeIcons.vial,
                          size: 35,
                        ),
                        Container(
                          padding: EdgeInsets.only(left: 10),
                          child: Column(
                            children: [
                              Text(
                                "${user.user != null ? user.bloodType ?? "-" :"-"}",
                                style: TextStyle(
                                    fontWeight: FontWeight.w600, fontSize: 15),
                              ),
                              const Text(
                                "Blood Type",
                                style: TextStyle(
                                    fontWeight: FontWeight.w300, fontSize: 12),
                              ),
                            ],
                          ),
                        )
                      ],
                    )),
                  ],
                ),
              ),
              user.user != null ?
                Container(
                  padding: const EdgeInsets.only( top: 30, left: 20, right: 20 ),
                  child: Column(
                    children: [
                      Row(
                        children: [
                          Flexible(child:tag( user.getWeightStatus(), weightStatusColoMap[ user.weightStatus ] )),
                          user.getRecommendAppointment() ?
                          Flexible(child: tag( "Medical Check", Colors.blueAccent)) :Container(),

                        ],
                      ),
                      Row(
                        children: [
                          user.getAbuseStatus() ?
                          tag( "watch your substance intake", Colors.orange )
                              : Container(),
                        ],
                      )
                    ],
                  )
                ) : Container()
            ],
          ),

        ));
  }
  Widget tag( String title, Color color ) {
    return Card(
      elevation: 15,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular( 20 )
      ),
      color: color,
      child: Padding(
        padding: const EdgeInsets.all( 5 ),
        child: Text(
          title,
          style: TextStyle( color: Colors.white ),
        ),
      )
    );
  }
}
