import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:medrecord/Managers/data_manager.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:provider/provider.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

class AppointmentForm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AppointmentFormState();
}

class _AppointmentFormState extends State<AppointmentForm> {
  DateTime date;
  String hospital;
  String appointmentType;

  List<String> hospitals = ["Santa Fe", "Clínica del Country", "Reina Sofía"];
  List<String> appointmentTypes = [
    "General",
    "Urgencias",
    "Prioritaria",
    "Especializada"
  ];

  @override
  Widget build(BuildContext context) {
    UserViewModel user = Provider.of<UserViewModel>(context);

    return Container(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Select Hospital",
              style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.bold) //style: TextStyle(),
              ),
          Row(
            children: [
              Expanded(
                child: DropdownButton<String>(
                  value: hospital,
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  //style: TextStyle( color: Theme.of(context).primaryColor ),
                  underline: Container(
                    height: 2,
                    color: Theme.of(context).primaryColor,
                  ),
                  onChanged: (String value) {
                    setState(() {
                      hospital = value;
                    });
                  },
                  items:
                      hospitals.map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              )
            ],
          ),
          Text("Select Type",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
          Row(
            children: [
              Expanded(
                child: DropdownButton<String>(
                  value: appointmentType,
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  //style: TextStyle( color: Theme.of(context).primaryColor ),
                  underline: Container(
                    height: 2,
                    color: Theme.of(context).primaryColor,
                  ),
                  onChanged: (String value) {
                    setState(() {
                      appointmentType = value;
                    });
                  },
                  items: appointmentTypes
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
              )
            ],
          ),
          Text(
              //should we keep this text?
              "Select a Date",
              style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold)),
          RaisedButton(
            //elevation: 8,
            child: Text(
              "Date",
              style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                  fontWeight: FontWeight.w100),
            ),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            color: Theme.of(context).primaryColor,
            onPressed: () {
              DatePicker.showDatePicker(context,
                  showTitleActions: true,
                  minTime: DateTime.now(),
                  maxTime: DateTime(2021, 12, 31), onChanged: (pDate) {
                date = pDate;
              }, onConfirm: (pDate) {
                date = pDate;
              }, currentTime: DateTime.now(), locale: LocaleType.en);
            },
          ),
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Row(
              children: [
                Expanded(
                  child: RaisedButton(
                    child: Text(
                      "Confirm",
                      style: TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w400,
                          fontSize: 20),
                    ),
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10)),
                    color: Theme.of(context).primaryColor,
                    onPressed: () async {
                      var connectivityResult =
                          await(Connectivity().checkConnectivity());
                      if (connectivityResult != ConnectivityResult.mobile &&
                          connectivityResult != ConnectivityResult.wifi) {
                        showDialog<void>(
                            context: context,
                            barrierDismissible: true,
                            builder: (BuildContext context) {
                              return AlertDialog(
                                title: Text("No connection"),
                                content: Text("Please try again later"),
                                //content: Icon(LineAwesomeIcons.exclamation_triangle, color: Colors.red, size: 60,),
                                actions: <Widget>[
                                  TextButton(
                                    child: Text(
                                      'OK',
                                      style: TextStyle(fontSize: 20),
                                    ),
                                    onPressed: () {
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              );
                            });
                      } else {
                        date == null ||
                                hospital == null ||
                                appointmentType == null
                            ? showDialog<void>(
                                context: context,
                                barrierDismissible: true,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    title: Text("Incomplete Information!"),
                                    content:
                                        Text("Please fill in missing fields"),
                                    actions: <Widget>[
                                      TextButton(
                                        child: Text(
                                          'OK',
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                },
                              )
                            : _saveAppointment(user);
                        Navigator.of(context).pop();
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  void _saveAppointment(UserViewModel user) async {
    Map<String, dynamic> appointment = {
      "date": Timestamp.fromDate(date),
      "hospital": hospital,
      "type": appointmentType,
    };
    await DataManager.saveAppointment(user, appointment);
  }
}
