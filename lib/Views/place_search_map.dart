import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:medrecord/Models/place.dart';
import 'package:medrecord/ViewModels/services/geolocator_service.dart';
import 'package:medrecord/ViewModels/services/marker_map_service.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

class MapSearch extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final currentPosition = Provider.of<Position>(context);
    final placesProvider = Provider.of<Future<List<Place>>>(context);
    final geoService = GeoLocatorService();
    final markerService = MarkerService();

    return FutureProvider(
      create: (context) => placesProvider,
      child: Scaffold(
        body: (currentPosition != null)
            ? Consumer<List<Place>>(builder: (_, places, __) {
          var markers = (places != null)
              ? markerService.getMarkers(places)
              : List<Marker>();
          return (places != null)
              ? Container(
              padding: const EdgeInsets.only(top: 60),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 5, right: 20),
                        child: Row(
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(right: 10),
                              child: IconButton(
                                  icon: Icon(
                                    LineAwesomeIcons.angle_left,
                                    size: 30,
                                    color: Theme.of(context)
                                        .primaryColor,
                                  ),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  }),
                            ),
                            Text(
                              "Nearby Hospitals",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  fontSize: 28,
                                  color:
                                  Theme.of(context).primaryColor),
                            )
                          ],
                        ),
                      ),
                    ),
                    Container(
                      height: MediaQuery.of(context).size.height / 3,
                      width: MediaQuery.of(context).size.width,
                      child: GoogleMap(
                        initialCameraPosition: CameraPosition(
                            target: LatLng(currentPosition.latitude,
                                currentPosition.longitude),
                            zoom: 16.0),
                        zoomGesturesEnabled: true,
                        markers: Set<Marker>.of(markers),
                      ),
                    ),
                    const SizedBox(
                      height: 2,
                    ),
                    Expanded(
                        child: Padding(
                            padding:
                            const EdgeInsets.only(left: 15, right: 15),
                            child: ListView.builder(
                              itemCount: places.length,
                              itemBuilder: (context, index) {
                                return FutureProvider(
                                    create: (context) =>
                                        geoService.getDistance(
                                            currentPosition.latitude,
                                            currentPosition.longitude,
                                            places[index]
                                                .geometry
                                                .location
                                                .lat,
                                            places[index]
                                                .geometry
                                                .location
                                                .lng),
                                    child: Padding(
                                      padding:
                                      const EdgeInsets.only(bottom: 6),
                                      child: Card(
                                        shape: RoundedRectangleBorder(
                                            borderRadius:
                                            BorderRadius.circular(
                                                10)),
                                        elevation: 5,
                                        child: ListTile(
                                          title: Text(
                                            places[index].name,
                                            style: TextStyle(
                                              fontWeight:
                                              FontWeight.w600,
                                              fontSize: 18,
                                            ),
                                          ),
                                          subtitle: Column(
                                            crossAxisAlignment:
                                            CrossAxisAlignment
                                                .start,
                                            children: [
                                              (places[index].rating !=
                                                  null)
                                                  ? Row(
                                                children: [
                                                  RatingBarIndicator(
                                                    rating: places[
                                                    index]
                                                        .rating,
                                                    itemBuilder: (context, index) => Icon(
                                                        Icons
                                                            .star,
                                                        color: Colors
                                                            .amber),
                                                    itemCount:
                                                    5,
                                                    itemSize:
                                                    15,
                                                  )
                                                ],
                                              )
                                                  : Row(),
                                              Consumer<double>(
                                                  builder: (context,
                                                      meters,
                                                      widget) {
                                                    return (meters !=
                                                        null)
                                                        ? Text(
                                                        "${places[index].vicinity} \u00b7 ${(meters / 100).round()} km")
                                                        : Container();
                                                  })
                                            ],
                                          ),
                                          trailing: IconButton(
                                            icon: Icon(
                                                LineAwesomeIcons
                                                    .directions),
                                            color: Theme.of(context)
                                                .primaryColor,
                                            onPressed: () {
                                              _launchMapsUrl(
                                                  places[index]
                                                      .geometry
                                                      .location
                                                      .lat,
                                                  places[index]
                                                      .geometry
                                                      .location
                                                      .lng);
                                            },
                                          ),
                                        ),
                                      ),
                                    ));
                              },
                            )))
                  ]))
              : Center(
            child: CircularProgressIndicator(),
          );
        })
            : Center(
          child: CircularProgressIndicator(),
        ),
      ),
    );
  }

  void _launchMapsUrl(double lat, double lng) async {
    final url = "https://www.google.com/maps/search/?api=1&query=$lat,$lng";
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw "Could not launch $url";
    }
  }
}