import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
//import 'package:flutter_splash/flutter_splash.dart';

class Splash extends StatelessWidget {
  final double padValue = 0;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: Center(
            child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Text(
                  "MedRecord",
                  style: TextStyle(
                      fontFamily: "Poppins",
                      fontWeight: FontWeight.w700,
                      color: Colors.white,
                      fontSize: 45),
                )),
            Text(
              "Your electronic health record",
              style: TextStyle(
                  fontFamily: "Poppins",
                  fontWeight: FontWeight.w200,
                  color: Colors.white,
                  fontSize: 15),
            )
          ],
        )),
        backgroundColor: HexColor("#0063AB"),
      ),
    );
  }
}
