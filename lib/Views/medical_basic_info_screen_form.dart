import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:medrecord/Managers/data_manager.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:medrecord/Views/home.dart';
import 'package:medrecord/util/response_messages.dart';
import 'package:provider/provider.dart';

class MedicalBasicInfoForm extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => _MedicalBasicInfoFormState();

}
class _MedicalBasicInfoFormState extends State<MedicalBasicInfoForm>{

  int _age;
  String _blood;
  String _gender;
  int _height;
  String _lastName;
  String _name;
  int _weight;
  List<String> _genders = [ "M","F" ];
  List<String> _bloodTypes = [ "O+", "O-", "A+", "A-", "AB+", "AB-" ];
  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserViewModel>( context );
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric( vertical: 50, horizontal: 30 ),
          child: Card(
            elevation: 8,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular( 10 )
            ),
            child: Container(
              margin: const EdgeInsets.all(20),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Center(
                    child: Text(
                      "Please fill out your basic information",
                      style: TextStyle(
                        fontSize: 18,
                        color: Theme.of( context ).primaryColor
                      ),
                    ),
                  ),
                  const SizedBox( height: 10 ),
                  const Divider(),
                  const SizedBox( height: 10 ),
                  const Text(
                    "Name",
                    style: TextStyle( fontSize: 15 ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                        hintText: user.user == null ? "Howard" :user.fullname?.split(" ")[0] ?? 'Howard'
                    ),
                    onChanged: ( String value ) => this._name = value,
                  ),
                  const Text(
                    "Last Name",
                    style: TextStyle( fontSize: 15 ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                        hintText: user.user == null ? "Roark" : user.fullname?.split(" ")[1] ??'Roark'
                    ),
                    onChanged: ( String value ) => this._lastName = value,
                  ),
                  const Text(
                    "Age",
                    style: TextStyle( fontSize: 15 ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                      hintText: "${user.user == null ? "35" :user.age ?? '35'}"
                    ),
                    keyboardType: TextInputType.number,
                    onChanged: ( value ) => this._age = int.parse( value ),
                  ),
                  const Text(
                    "Height",
                    style: TextStyle( fontSize: 15 ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                        hintText: "${user.user == null ? "175 cm" : user.height ?? '175 cm'}"
                    ),
                    keyboardType: TextInputType.number,
                    onChanged: ( value ) => this._height = int.parse( value ),
                  ),
                  const Text(
                    "Weight",
                    style: TextStyle( fontSize: 15 ),
                  ),
                  TextField(
                    decoration: InputDecoration(
                        hintText: "${user.user == null ? "70 kg" : user.weight ?? '70 kg'}"
                    ),
                    keyboardType: TextInputType.number,
                    onChanged: ( String value ) => this._weight = int.parse( value ),
                  ),
                  const Text(
                    "Blood Type",
                    style: TextStyle( fontSize: 15 ),
                  ),
                  DropdownButton<String>(
                    value: user.user == null ? this._bloodTypes.first: user.bloodType ?? this._blood ?? this._bloodTypes.first,
                    icon: Icon( Icons.arrow_downward ),
                    iconSize: 24,
                    elevation: 16,
                    underline: Container(
                      height: 2,
                      color: Theme.of( context ).primaryColor,
                    ),
                    onChanged: ( String value ) => setState(() => this._blood = value),
                    items: this._bloodTypes.map<DropdownMenuItem<String>>(( String value ) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text( value ),
                      );
                    }).toList(),
                  ),
                  const Text(
                    "Gender",
                    style: TextStyle( fontSize: 15 ),
                  ),
                  DropdownButton<String>(
                    value: this._gender ?? this._genders.first,
                    icon: Icon( Icons.arrow_downward ),
                    iconSize: 24,
                    elevation: 16,
                    underline: Container(
                      height: 2,
                      color: Theme.of( context ).primaryColor,
                    ),
                    onChanged: ( String value ) => setState(() => this._gender = value),
                    items: this._genders.map<DropdownMenuItem<String>>(( String value ) {
                      return DropdownMenuItem<String>(
                        value: value,
                        child: Text( value ),
                      );
                    }).toList(),
                  ),
                  const SizedBox( height: 10 ),
                  Center(
                    child: RaisedButton(
                      child: const Text(
                        "Confirm",
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w400,
                            fontSize: 20
                        ),
                      ),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10)),
                      color: Theme.of(context).primaryColor,
                      onPressed: () async {
                        var connectivityResult = await ( Connectivity().checkConnectivity() );
                        if ( connectivityResult != ConnectivityResult.mobile && connectivityResult != ConnectivityResult.wifi ){
                          showDialog<void>(
                              context: context,
                              barrierDismissible: true,
                              builder: (
                                  BuildContext context) {
                                return AlertDialog(
                                  title: const Text( "No connection" ),
                                  content: const Text( "Please try again later" ),
                                  //content: Icon(LineAwesomeIcons.exclamation_triangle, color: Colors.red, size: 60,),
                                  actions: <Widget>[
                                    TextButton(
                                      child: const Text(
                                        'OK',
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              }
                          );
                        }
                        else{
                          var response = await updateUser( context, Provider.of<UserViewModel>( context, listen: false ));
                          if( response == ResponseStatus.SUCCESS ) {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => Home()));
                          }
                          else if( response == ResponseStatus.CONNECTIVITY ) {
                            showDialog<void>(
                                context: context,
                                barrierDismissible: true,
                                builder: (
                                    BuildContext context) {
                                  return AlertDialog(
                                    title: const Text( "No connection" ),
                                    content: const Text( "Please try again later" ),
                                    //content: Icon(LineAwesomeIcons.exclamation_triangle, color: Colors.red, size: 60,),
                                    actions: <Widget>[
                                      TextButton(
                                        child: const Text(
                                          'OK',
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                }
                            );
                          }
                          else {
                            showDialog<void>(
                                context: context,
                                barrierDismissible: true,
                                builder: (
                                    BuildContext context) {
                                  return AlertDialog(
                                    title: const Text( "Error" ),
                                    content: Text( response ),
                                    //content: Icon(LineAwesomeIcons.exclamation_triangle, color: Colors.red, size: 60,),
                                    actions: <Widget>[
                                      TextButton(
                                        child: const Text(
                                          'OK',
                                          style: TextStyle(fontSize: 20),
                                        ),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                    ],
                                  );
                                }
                            );
                          }
                        }
                      },
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
  Future<String> updateUser( BuildContext context, UserViewModel user ) async {
    Map<String,dynamic> userMap = {
      "age": this._age ??= user.user == null ? 35 : user.age ?? 35,
      "blood": this._blood ??= user.user == null ? _bloodTypes.first : user.bloodType ?? _bloodTypes.first,
      "gender": this._gender ??= _genders.first,
      "height": this._height ??= user.user == null ? 175 : user.height ?? 175,
      "image": user.user == null ? "https://picsum.photos/200/300.jpg" : user.photo ?? "https://picsum.photos/200/300.jpg",
      "lastName": this._lastName ??= user.user == null ? "Figueroa" : user.fullname?.split(" ")[ 1 ] ?? "Figueroa",
      "name": this._name ??= user.user == null ? "Anderson" : user.fullname?.split(" ")[ 0 ] ?? "Anderson",
      "weight": this._weight ??= user.user == null ? 70 : user.weight ?? 70,
    };
    return await DataManager.updateUser( user, userMap );
  }

}