import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:medrecord/Views/record_screen.dart';
import 'package:medrecord/Views/services_screen.dart';

import '../main.dart';
import 'appointments_screen.dart';

class Home extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeState();
}

class _HomeState extends State<Home>{

  int _selectedIndex = 1;

  List<Widget> _widgetOptions = [
    RecordScreen(),
    ServicesScreen(),
    AppointmentsScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
            "Home",
          //style: TextStyle(color: Colors.),
        ),
        backgroundColor: Theme.of( context ).primaryColor,
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              decoration: BoxDecoration(
                color: Theme.of( context ).primaryColor,
              ),
              child: Center(
                child: const Text(
                  "MedRecord",
                  style: TextStyle( color: Colors.white, fontSize: 25, fontWeight: FontWeight.w700 ),
                ),
              )
            ),
            ListTile(
              title: Text(
                "Logout",
                style: TextStyle( fontSize: 18, color: Theme.of( context ).primaryColor  ),
              ),
              onTap: () async {
                await FirebaseAuth.instance.signOut();
                Navigator.of( context ).pushAndRemoveUntil(
                    MaterialPageRoute( builder: ( context ) => MyApp() ),
                    ( route ) => false );
              },
            ),
          ],
        ),
      ),
      body: _widgetOptions[_selectedIndex], //elementAt si no sirve
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(LineAwesomeIcons.book), label: "Record"),
          BottomNavigationBarItem(
              icon: Icon(LineAwesomeIcons.medkit), label: "Services"),
          BottomNavigationBarItem(
              icon: Icon(LineAwesomeIcons.calendar_check),
              label: "Appointments"),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Theme.of(context).primaryColor,
        onTap: (index) {
          setState(() {
            this._selectedIndex = index;
          });
        },
      ),
    );
  }
}
