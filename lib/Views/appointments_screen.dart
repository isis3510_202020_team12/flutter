import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:medrecord/Views/widgets/appointment_form.dart';
import 'package:provider/provider.dart';
import 'package:intl/intl.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class AppointmentsScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() => AppointmentScreenState();

}

class AppointmentScreenState extends State<AppointmentsScreen>{

  final scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    UserViewModel user = Provider.of<UserViewModel>( context );

    return Scaffold(
      key: scaffoldKey,
      body:  Container(
        padding: const EdgeInsets.only( top: 10, left: 20, right: 20 ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Appointments",
              style: TextStyle(
                  fontWeight: FontWeight.w700,
                  fontSize: 35,
                  color: Theme.of( context ).primaryColor ) ,
            ),
            user.user != null ?
              user.appointments != null ?
                Container(
                  height: MediaQuery.of( context ).size.height * 0.7,
                  child: ListView.builder(
                    itemCount: user.appointments.length,
                    itemBuilder: (BuildContext context, int index) {
                      return Container(
                        padding: const EdgeInsets.all(5),
                        child: Card(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          elevation: 5,
                          child: ListTile(
                              title: Text(
                                DateFormat.yMMMMEEEEd().format( DateTime.parse( user.appointments[ index ].date.toDate().toString() ) )
                                ,
                                style: const TextStyle(
                                  fontWeight: FontWeight.w700,
                                  fontSize: 20,
                                ),
                              ),
                              subtitle: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      user.appointments[index].type
                                  ),
                                  Text(
                                      user.appointments[index].hospital
                                  ),
                                ],
                              )
                          ),
                        ),
                      );
                    },
                  ),
                )
                  :
                Container()
              :
                Container()
          ],
        ),
      )
      ,
      floatingActionButton: FloatingActionButton(
        backgroundColor: Theme.of(context).primaryColor,
        child: Icon(Icons.add),
        onPressed: () async {
          var connectivityResult = await ( Connectivity().checkConnectivity() );
          if ( connectivityResult != ConnectivityResult.mobile && connectivityResult != ConnectivityResult.wifi ){
            showDialog<void>(
              context: context,
              barrierDismissible: true,
              builder: (
                  BuildContext context) {
                return AlertDialog(
                  title: const Text( "No connection" ),
                  content: const Text( "Please try again later" ),
                  //content: Icon(LineAwesomeIcons.exclamation_triangle, color: Colors.red, size: 60,),
                  actions: <Widget>[
                    TextButton(
                      child: const Text(
                        'OK',
                        style: TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                    ),
                  ],
                );
              }
            );
          }
          else {
            Alert(
                context: context,
                title: "Schedule an Appointment",
                content: Container(
                  child: Column(
                    children: [
                      Divider(),
                      AppointmentForm()
                    ],
                  ),
                ),
                buttons: []
            ).show();
          }
        },
      ),
    );
  }

}