import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:medrecord/Models/treatment.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:provider/provider.dart';

class TreatmentFormScreen extends StatefulWidget{

  @override
  State<StatefulWidget> createState() => _TreatmentFormState( );
}

class _TreatmentFormState extends State<TreatmentFormScreen>{
  List<String> types = [ "Medicament","Injection" ];
  Treatment _treatment = Treatment( type: "Medicament");

  @override
  Widget build(BuildContext context) {
    UserViewModel user = Provider.of<UserViewModel>( context );

    return Scaffold(
      body: SingleChildScrollView(
        child: Card(
          margin: const EdgeInsets.symmetric(vertical: 50, horizontal: 20),
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular( 10 )
          ),
          child: Container(
            padding: const EdgeInsets.symmetric( horizontal: 10 ),
            child: Column(
              children: [
                Text(
                  "Treatment Form",
                  style: TextStyle(
                      fontSize: 25,
                      color: Theme.of( context ).primaryColor
                  ),
                ),
                SafeArea(child: Container(height: 0)),
                const Text(
                  "Please fill in the following form to add a treatment to your diagnosis",
                  style: TextStyle(
                      fontSize: 15
                  ),
                ),
                const Divider(),
                TextField(
                    decoration: InputDecoration(
                        hintText: "your treatment name",
                        labelText: "Name"
                    ),
                    onChanged: ( name ) {
                      setState(() {
                        this._treatment.name = name;
                      });
                    }
                ),
                Row(
                  children: [
                    Expanded(
                      child: DropdownButton<String>(
                        value: this._treatment.type,
                        icon: Icon( Icons.arrow_downward ),
                        iconSize: 24,
                        elevation: 16,
                        //style: TextStyle( color: Theme.of(context).primaryColor ),
                        underline: Container(
                          height: 2,
                          color: Theme.of( context ).primaryColor,
                        ),
                        onChanged: ( String value ) {
                          setState(() {
                            this._treatment.type = value;
                          });
                        },
                        items: types.map<DropdownMenuItem<String>>(( String value ) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text( value ),
                          );
                        }).toList(),
                      ),
                    )
                  ],
                ),
                TextField(
                    decoration: const InputDecoration(
                        hintText: "X pills",
                        labelText: "Dosage"
                    ),
                    onChanged: ( value ) {
                      setState(() {
                        this._treatment.dosage = value;
                      });
                    }
                ),
                TextField(
                    decoration: const InputDecoration(
                        hintText: "X months",
                        labelText: "Duration"
                    ),
                    onChanged: ( value ) {
                      setState(() {
                        this._treatment.duration = value;
                      });
                    }
                ),
                TextField(
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                        labelText: "Total number of doses"
                    ),
                    onChanged: ( value ) {
                      setState(() {
                        this._treatment.totalDoses = int.parse( value );
                      });
                    }
                ),
                TextField(
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                        labelText: "Doses already taken"
                    ),
                    onChanged: ( value ) {
                      setState(() {
                        this._treatment.dosesTaken = int.parse( value );
                      });
                    }
                ),
                SafeArea(child: Container(height: 0)),
                Row(
                  children: [
                    Expanded(
                      child: Column(
                        children: [
                          Center(
                            child: const Text(
                                "Select the prescription date",
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold
                                )
                            ),
                          ),
                          RaisedButton(
                            //elevation: 8,
                            child: const Text(
                              "Calendar",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w100
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            color: Theme.of( context ).primaryColor,
                            onPressed: () {
                              DatePicker.showDatePicker(
                                  context,
                                  showTitleActions: true,
                                  minTime: DateTime( 1998, 0, 0 ),
                                  maxTime: DateTime.now(),
                                  onChanged: ( date ) {
                                    this._treatment.prescriptionDate = Timestamp.fromDate( date );
                                  },
                                  onConfirm: ( date ) {
                                    this._treatment.prescriptionDate = Timestamp.fromDate( date );
                                  },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.en
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child: Column(
                        children: [
                          Center(
                            child: const Text(
                                "Select the starting date",
                                style: TextStyle(
                                    fontSize: 15,
                                    fontWeight: FontWeight.bold
                                )
                            ),
                          ),
                          RaisedButton(
                            //elevation: 8,
                            child: const Text(
                              "Calendar",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontSize: 20,
                                  fontWeight: FontWeight.w100
                              ),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            color: Theme.of( context ).primaryColor,
                            onPressed: () {
                              DatePicker.showDatePicker(
                                  context,
                                  showTitleActions: true,
                                  minTime: DateTime( 1998, 0, 0 ),
                                  maxTime: DateTime.now(),
                                  onChanged: ( date ) {
                                    this._treatment.startDate = Timestamp.fromDate( date );
                                  },
                                  onConfirm: ( date ) {
                                    this._treatment.startDate = Timestamp.fromDate( date );
                                  },
                                  currentTime: DateTime.now(),
                                  locale: LocaleType.en
                              );
                            },
                          ),
                        ],
                      ),
                    )
                  ],
                ),
                TextField(
                    decoration: const InputDecoration(
                        hintText: "every X hours, everyday",
                        labelText: "Frecuency"
                    ),
                    onChanged: ( value ) {
                      setState(() {
                        this._treatment.times = value;
                      });
                    }
                ),
                TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines : null,
                  decoration: const InputDecoration(
                      labelText: "Comments"),
                  onChanged: ( value ) {
                    setState(() {
                      this._treatment.comments = value;
                    });
                  },
                ),
                SafeArea(child: Container(height: 0)),
                Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 50,
                        child: RaisedButton(
                          child: Text(
                            "Clear",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontWeight: FontWeight.w400,
                                fontSize: 20
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          color: Colors.white,
                          onPressed: () {
                            this._treatment = Treatment();
                          },
                        ),
                      ),
                    ),
                    this._treatment.name != null ?
                    Expanded(
                      child: Container(
                        height: 50,
                        child: RaisedButton(
                          child: const Text(
                            "Confirm",
                            style: TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w400,
                                fontSize: 20
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10)),
                          color: Theme.of(context).primaryColor,
                          onPressed: ()  {
                            user.addTreatmentToCurrentDiagnose( this._treatment );
                            Navigator.pop( context );
                          },
                        ),
                      ),
                    )
                        :
                    Container(),
                  ],
                ),
                SafeArea(child: Container(height: 0)),
              ],
            ),
          ),
        ),
      )
    );
  }

}