import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:line_awesome_flutter/line_awesome_flutter.dart';
import 'package:medrecord/Models/diagnosis.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:medrecord/Views/treatment_form.dart';
import 'package:medrecord/util/response_messages.dart';
import 'package:provider/provider.dart';
import "package:medrecord/Managers/data_manager.dart";

import 'home.dart';

class DiagnoseFormScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _DiagnoseFormState();
}

class _DiagnoseFormState extends State<DiagnoseFormScreen> {
  @override
  Widget build(BuildContext context) {
    UserViewModel user = Provider.of<UserViewModel>(context);
    return Scaffold(
        body: SingleChildScrollView(
      child: Card(
          margin: const EdgeInsets.only(top: 50, bottom: 50, left: 20, right: 20),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          elevation: 2,
          child: Container(
            height: MediaQuery.of(context).size.height * 0.95,
            padding: const EdgeInsets.all(20),
            child: Column(
              children: [
                Text(
                  "Diagnosis Form",
                  style: TextStyle(
                      fontSize: 25, color: Theme.of(context).primaryColor),
                ),
                SafeArea(child: Container(height: 0)),
                const Text(
                  "Please fill in the following form to add a diagnosis to your record",
                  style: TextStyle(fontSize: 15),
                ),
                Divider(),
                TextField(
                    decoration: InputDecoration(
                        hintText: "your diagnosis name", labelText: "Name"),
                    onChanged: (name) {
                      setState(() {
                        user.currentDiagnose.name = name;
                      });
                    }),
                TextField(
                  keyboardType: TextInputType.multiline,
                  maxLines: null,
                  decoration: InputDecoration(
                      hintText: "a detailed description",
                      labelText: "Description"),
                  onChanged: (description) {
                    setState(() {
                      user.currentDiagnose.description = description;
                    });
                  },
                ),
                SafeArea(child: Container(height: 0)),
                Text(
                  "Treatments",
                  style: TextStyle(
                      fontSize: 20, color: Theme.of(context).primaryColor),
                ),
                user.currentDiagnose.treatments != null
                    ? Container(
                        height: MediaQuery.of(context).size.height * 0.15,
                        width: MediaQuery.of(context).size.width,
                        child: _showTreatmentsList(context, user),
                      )
                    : Container(),
                //SafeArea(child: Container(height: 0)),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                          child: Container(
                        height: 50,
                        child: ButtonTheme(
                          child: RaisedButton.icon(
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            elevation: 2,
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          TreatmentFormScreen()));
                            },
                            label: const Text(
                              'Add Treatment',
                              style: TextStyle(
                                  fontWeight: FontWeight.w600, fontSize: 20),
                            ),
                            icon: Icon(
                              LineAwesomeIcons.calendar_plus,
                              size: 35,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ),
                      )),
                    ],
                  ),
                ),
                //SafeArea(child: Container(height: 10)),
                Expanded(
                  child: Row(
                    children: [
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.075,
                          child: RaisedButton(
                            child: Text(
                              "Clear",
                              style: TextStyle(
                                  color: Theme.of(context).primaryColor,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 20),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            color: Colors.white,
                            onPressed: () {
                              user.currentDiagnose = Diagnosis();
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.075,
                          child: RaisedButton(
                            child:const  Text(
                              "Confirm",
                              style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontSize: 20),
                            ),
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10)),
                            color: Theme.of(context).primaryColor,
                            onPressed: () async {
                              var connectivityResult =
                                  await (Connectivity().checkConnectivity());
                              if (connectivityResult !=
                                      ConnectivityResult.mobile &&
                                  connectivityResult !=
                                      ConnectivityResult.wifi) {
                                showDialog<void>(
                                    context: context,
                                    barrierDismissible: true,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: const Text("No connection"),
                                        content: const Text("Please try again later"),
                                        //content: Icon(LineAwesomeIcons.exclamation_triangle, color: Colors.red, size: 60,),
                                        actions: <Widget>[
                                          TextButton(
                                            child: const Text(
                                              'OK',
                                              style: TextStyle(fontSize: 20),
                                            ),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                          ),
                                        ],
                                      );
                                    });
                              } else {
                                String response = await _sendDiagnosis(user);

                                if (response == ResponseStatus.SUCCESS) {
                                  showDialog<void>(
                                    context: context,
                                    barrierDismissible: true,
                                    builder: (BuildContext context) {
                                      return AlertDialog(
                                        title: const Text(ResponseStatus.SUCCESS),
                                        content: const Text(
                                            "your diagnosis has been saved succesfully"),
                                        actions: <Widget>[
                                          TextButton(
                                            child: const Text(
                                              'OK',
                                              style: TextStyle(fontSize: 20),
                                            ),
                                            onPressed: () {
                                              Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Home()));
                                            },
                                          ),
                                        ],
                                      );
                                    },
                                  );
                                }
                              }
                            },
                          ),
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          )),
    ));
  }

  Widget _showTreatmentsList(BuildContext context, UserViewModel user) {
    return ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: user.currentDiagnose.treatments.length,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            children: [
              Expanded(
                child: Card(
                    margin: const EdgeInsets.all(5),
                    elevation: 6,
                    child: Container(
                        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
                        child: Column(
                          children: [
                            Center(
                              child: Text(
                                user.currentDiagnose.treatments[index].name,
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.bold),
                              ),
                            ),
                            Center(
                              child: Text(
                                user.currentDiagnose.treatments[index].type,
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                            ),
                            Center(
                              child: Text(
                                user.currentDiagnose.treatments[index].times,
                                style: TextStyle(
                                  fontSize: 15,
                                ),
                              ),
                            )
                          ],
                        ))),
              )
            ],
          );
        });
  }

  Future<String> _sendDiagnosis(UserViewModel user) async {
    return DataManager.saveDiagnosis(user);
  }
}
