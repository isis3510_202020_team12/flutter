import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';
import 'package:hive/hive.dart';
import 'package:medrecord/Models/appointment.dart';
import 'package:medrecord/Models/diagnosis.dart';
import 'package:medrecord/Models/treatment.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:medrecord/util/hive_keys.dart';
import 'package:connectivity/connectivity.dart';
import 'package:medrecord/util/response_messages.dart';

class DataManager{
  static Future<bool> checkAndLoadData( UserViewModel user ) async {
    var box = await Hive.openBox<Map<String,dynamic>>( HiveKeys.USER );
    if ( box.isNotEmpty ){
      String uuid = box.keys.first;
      box.close();
      //if there is conection, fetch data, else, show persisted
      var connectivityResult = await ( Connectivity().checkConnectivity() );
      if ( connectivityResult == ConnectivityResult.mobile || connectivityResult == ConnectivityResult.wifi ){
        await fetchAndSaveData( user, uuid );
        return true;
      }
      else{// if the token exists but there is no conection
        loadPersisted( user );
      }
      return true;
    }
    else {
      return false;
    }
  }

  static Future<void> fetchAndSaveData( UserViewModel user, String uuid ) async {
    final userCollection = FirebaseFirestore.instance.collection('users').doc( FirebaseAuth.instance.currentUser.uid.toString() );
    final userData = await userCollection.get();
    if( userData.data() == null ) {
      return;
    }
    user.fromJson( userData.data() );
    user.uuid = FirebaseAuth.instance.currentUser.uid.toString();
    QuerySnapshot appointmentData = await userCollection.collection("appointments").get();
    List<Appointment> appointments = [];
    for( int i = 0; i < appointmentData.docs.length; i++ ) {
      appointments.add( Appointment.fromJson( Map<String,dynamic>.from( appointmentData.docs[ i ].data() ) ) );
    }
    user.appointments = appointments;
    QuerySnapshot diagnosisData = await userCollection.collection("diagnosis").get();
    List<Diagnosis> diagnosis = [];
    List<Treatment> treatments = [];
    Treatment treatment;
    QuerySnapshot treatmentData;
    Diagnosis diagnose;
    for( int i = 0; i < diagnosisData.docs.length; i++ ) {
      diagnose = Diagnosis.fromJson(Map<String, dynamic>.from(diagnosisData.docs[ i ].data() ) );
      treatmentData = await diagnosisData.docs[ i ].reference.collection("treatments").get();
      treatments = [];
      for( int j = 0; j < treatmentData.docs.length; j++ ) {
        treatment = Treatment.fromJson( Map<String, dynamic>.from( treatmentData.docs[ j ].data() ) );
        treatments.add( treatment );
      }
      diagnose.treatments = treatments;
      diagnosis.add( diagnose );
    }
    user.diagnosis = diagnosis;
  }

  static Future<String> updateUser( UserViewModel user, Map<String, dynamic> userMap ) async {
    try {
      await FirebaseFirestore.instance
          .collection('users')
          .doc( FirebaseAuth.instance.currentUser.uid )
          .set(userMap);
      List<Diagnosis> diagnosis = user.diagnosis;
      List<Appointment> appointments = user.appointments;
      user.fromJson(userMap);
      user.diagnosis = diagnosis;
      user.appointments = appointments;
      return ResponseStatus.SUCCESS;
    } on SocketException catch (e,s) {
    FirebaseCrashlytics.instance.recordError( e, s );
    return ResponseStatus.CONNECTIVITY;
    }
    catch (e,s) {
    FirebaseCrashlytics.instance.recordError( e, s );
    return e.toString();
    }

  }

  static void loadPersisted( UserViewModel user ) async {
    var box = await Hive.openBox<Map<String,dynamic>>( HiveKeys.USER );
    var userMap = box.get( FirebaseAuth.instance.currentUser.uid.toString() );
    user.fromJson( userMap );
    box.close();
  }

  static void saveUser( UserViewModel user ) async {
    var box = await Hive.openBox<Map<String,dynamic>>( HiveKeys.USER );
    await box.put( FirebaseAuth.instance.currentUser.uid.toString(), user.toPersistenceJson() );
    box.close();
  }

  static void logout({String uuid}){
    var box = Hive.box<Map<dynamic,dynamic>>( HiveKeys.USER );
    box.delete( FirebaseAuth.instance.currentUser.uid.toString() );
    box.close();
  }
  
  static Future<String> saveDiagnosis( UserViewModel user ) async {
    user.currentDiagnose.registrationDate = Timestamp.fromDate( DateTime.now() );
    final userCollection = FirebaseFirestore.instance.collection( 'users' ).doc( FirebaseAuth.instance.currentUser.uid.toString() );
    final diagnosisCollection = userCollection.collection( "diagnosis" );
    Map<String, dynamic> diagnosisJson = user.currentDiagnose.toCollectionJson();
    try{
      DocumentReference reference = await diagnosisCollection.add( diagnosisJson );
      final singleCollection = diagnosisCollection.doc( reference.id );
      final treatmentCollection = singleCollection.collection( "treatments" );
      for(int i = 0; i < user.currentDiagnose.treatments.length ; i++ ){
        await treatmentCollection.add( user.currentDiagnose.treatments[ i ].toCollectionJson() );
      }
      user.addDiagnose( user.currentDiagnose );
      user.currentDiagnose = Diagnosis();
      return ResponseStatus.SUCCESS;
    }
    on SocketException catch(e,s){
      FirebaseCrashlytics.instance.recordError( e, s );
      return ResponseStatus.CONNECTIVITY;
    }
    catch(e,s){
      FirebaseCrashlytics.instance.recordError( e, s );
      return ResponseStatus.ERROR;
    }
  }

  static Future<String> saveAppointment( UserViewModel user, Map<String,dynamic> appointment ) async {
    final userCollection = FirebaseFirestore.instance.collection('users').doc( FirebaseAuth.instance.currentUser.uid.toString() );
    final appointmentCollection = userCollection.collection( "appointments" );
    user.addAppointment( Appointment.fromJson( appointment ) );
    try{
      await appointmentCollection.add( appointment );
      return ResponseStatus.SUCCESS;
    }
    on SocketException catch(e,s){
      FirebaseCrashlytics.instance.recordError( e, s );
      return ResponseStatus.CONNECTIVITY;
    }
    catch(e,s){
      FirebaseCrashlytics.instance.recordError( e, s );
      return ResponseStatus.ERROR;
    }
  }
}