import 'package:medrecord/Models/place.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class PlacesService {
  final key = 'AIzaSyANmCShluoD26bL5vqUvmB4qVGrVduJWr0';

  Future<List<Place>> getPlaces(double lat, double lng) async {
    try {
      var response = await http.get(
          'https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=$lat,$lng&type=hospital&rankby=distance&key=$key');
      var json = convert.jsonDecode(response.body);
      var jsonResults = json['results'] as List;
      return jsonResults.map((place) => Place.fromJson(place)).toList();
    } catch (e) {
      return null;
    }
  }
}
