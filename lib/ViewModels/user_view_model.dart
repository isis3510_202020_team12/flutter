import 'dart:math';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:medrecord/Models/appointment.dart';
import 'package:medrecord/Models/diagnosis.dart';
import 'package:medrecord/Models/treatment.dart';
import 'package:medrecord/Models/user.dart';

class UserViewModel extends ChangeNotifier {

  static const UNDERWEIGHT = 'Underweight';
  static const NORMAL = 'Normal Weight';
  static const OVERWEIGHT = 'Overweight';
  static const OBESE = 'Obese';

  User _user;
  Diagnosis _currentDiagnose;
  String _weightStatus;
  bool _abuseStatus;
  bool _recommendAppointment;


  UserViewModel();

  void fromJson( Map<String,dynamic> json ) {

    this._user = User.fromJson( json );
    this._currentDiagnose = Diagnosis();
    //this._weightStatus = getWeightStatus();
    //this._abuseStatus = getAbuseStatus();
    //this._recommendAppointment = getRecommendAppointment();
    notifyListeners();
  }
  
  Map<String,dynamic> toCollectionJson() => this._user.toCollectionJson();
  Map<String,dynamic> toPersistenceJson() =>  this._user.toPersistenceJson();
  

  User get user => this._user;
  String get uuid => this._user.uuid;
  String get fullname => this._user.fullname;
  int get age => this._user.age;
  int get height => this._user.height;
  int get weight => this._user.weight;
  String get bloodType => this._user.bloodType;
  String get photo => this._user.photo;
  List<Appointment> get appointments => this._user.appointments;
  List<Diagnosis> get diagnosis => this._user.diagnosis;
  Diagnosis get currentDiagnose => this._currentDiagnose;
  String get weightStatus => this._weightStatus;
  bool get abuseStatus => this._abuseStatus;
  bool get recommendAppointment => this._recommendAppointment;


  String getWeightStatus() {
    double imc = weight / pow(height / 100, 2 );
    String response = "";
    if( imc < 18.5 ) {
      response = UNDERWEIGHT;
    }
    else if( imc >= 18.5 && imc < 25 ) {
      response = NORMAL;
    }
    else if( imc >= 25 && imc < 30 ) {
      response = OVERWEIGHT;
    }
    else{
      response = OBESE;
    }
    this._weightStatus = response;
    return response;
  }

  bool getAbuseStatus() {
    if( diagnosis == null ) {
      return false;
    }
    for( int i = 0; i < diagnosis.length; i++ ) {
      if( diagnosis[ i ].name.toLowerCase().contains( "alcoholism" ) ) {
        return true;
      }
    }
    return false;
  }

  bool getRecommendAppointment() {
    if( appointments == null ) {
      return true;
    }
    DateTime now = DateTime.now();
    bool recommend = true;
    for( int i = 0; i < appointments.length; i++ ) {
      if( appointments[ i ].date.toDate().difference( now ).inDays < 30 ) {
        recommend = false;
        break;
      }
    }
    return recommend;
  }

  set uuid(String value) {
    this._user.uuid = value;
    notifyListeners();
  }

  set fullname(String value) {
    this._user.fullname = value;
    notifyListeners();
  }

  set age(int value) {
    this._user.age = value;
    notifyListeners();
  }

  set height(int value) {
    this._user.height = value;
    notifyListeners();
  }

  set weight(int value) {
    this._user.weight = value;
    notifyListeners();
  }

  set appointments( List<Appointment> value ){
    this._user.appointments = value;
    notifyListeners();
  }

  set diagnosis( List<Diagnosis> value ){
    this._user.diagnosis = value;
    notifyListeners();
  }

  set photo( String photo ){
    this._user.photo = photo;
    notifyListeners();
  }

  void addAppointment( Appointment appointment ){
    this.appointments == null ? this.appointments = [ appointment ] : this.appointments.add( appointment );
    notifyListeners();
  }

  void addDiagnose( Diagnosis diagnose ){
    this.diagnosis == null ? this.diagnosis = [ diagnose ] : this.diagnosis.add( diagnose );
    notifyListeners();
  }

  void addTreatment( Diagnosis diagnose, Treatment treatment ){
    diagnose.treatments == null ? diagnose.treatments = [ treatment ] : diagnose.treatments.add( treatment );
    notifyListeners();
  }

  set currentDiagnose( Diagnosis value ){
    this._currentDiagnose = value;
    notifyListeners();
  }

  void addTreatmentToCurrentDiagnose( Treatment treatment ){
    this._currentDiagnose.treatments == null ? this._currentDiagnose.treatments = [ treatment ] : this._currentDiagnose.treatments.add( treatment );
    notifyListeners();
  }

  void clean() {
    this._user = null;
  }

}
