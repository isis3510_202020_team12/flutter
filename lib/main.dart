import 'dart:isolate';

import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:medrecord/Models/place.dart';
import 'package:medrecord/ViewModels/services/places_service.dart';
import 'package:medrecord/ViewModels/user_view_model.dart';
import 'package:medrecord/Views/login_screen.dart';
import 'package:medrecord/ViewModels/services/geolocator_service.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_crashlytics/firebase_crashlytics.dart';

import 'Managers/data_manager.dart';
import 'Views/home.dart';
import 'Views/splash_screen.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Hive.initFlutter();
  runApp( MyApp() );
}

class MyApp extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  final locatorService = GeoLocatorService();
  final placesService = PlacesService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initialization,
      builder: (context, snapshot) {
        if (snapshot.hasError) {
          print("error flutterfire server");
        }
        if (snapshot.connectionState == ConnectionState.done) {
          FlutterError.onError = FirebaseCrashlytics.instance.recordFlutterError;
          Isolate.current.addErrorListener(RawReceivePort((pair) async {
            final List<dynamic> errorAndStacktrace = pair;
            await FirebaseCrashlytics.instance.recordError(
              errorAndStacktrace.first,
              errorAndStacktrace.last,
            );
          }).sendPort);
          //FirebaseCrashlytics.instance.crash();
          return MultiProvider(
              providers: [
                ChangeNotifierProvider(create: (context) => UserViewModel()),
                FutureProvider(
                    create: (context) => locatorService.getLocation()),
                ProxyProvider<Position, Future<List<Place>>>(
                  update: (context, position, places) {
                    return (position != null)
                        ? placesService.getPlaces(
                            position.latitude, position.longitude)
                        : null;
                  },
                )
              ],
              child: MaterialApp(
                title: 'Med Record',
                theme: ThemeData(
                  primaryColor: HexColor("#0063AB"),
                  fontFamily: 'Poppins',
                  focusColor: Colors.blue,
                ),
                home: Validator(),
                debugShowCheckedModeBanner: false,
              ));
        }
        //TODO fix splash to last for a minimun of 2 seconds
        return Splash();
      },
    );
  }
}
class Validator extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    UserViewModel user = Provider.of<UserViewModel>( context );
    return FutureBuilder<bool>(
      future: DataManager.checkAndLoadData( user ),
      builder: ( context, snapshot ){
        if( snapshot.hasData ){
          return snapshot.data ? Home() : LoginScreen();
        }
        else{
          return Splash();
        }
      },
    );
  }

}
